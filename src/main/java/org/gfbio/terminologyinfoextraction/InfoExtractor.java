package org.gfbio.terminologyinfoextraction;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAnnotationValue;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.UnloadableImportException;
import org.semanticweb.skos.SKOSAnnotation;
import org.semanticweb.skos.SKOSConcept;
import org.semanticweb.skos.SKOSConceptScheme;
import org.semanticweb.skos.SKOSCreationException;
import org.semanticweb.skos.SKOSDataset;
import org.semanticweb.skosapibinding.SKOSManager;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

/**
 * This class extracts information from an uploaded ontology and tells whether
 * it is an owl or a skos ontology.
 * 
 * @author alexandra
 * 
 */
public class InfoExtractor {

	private String terminology_url;
	private String extension;
	private HashMap<String, String> map = new HashMap<String, String>();
	private HashSet<String> contributors = new HashSet<String>();
	private HashSet<String> creators = new HashSet<String>();
	private HashSet<String> definitions = new HashSet<String>();
	private HashSet<String> labels = new HashSet<String>();
	private HashSet<String> synonyms = new HashSet<String>();
	private HashSet<SKOSAnnotation> skosAnnotations = new HashSet<SKOSAnnotation>();
	private StringBuilder creatorList = new StringBuilder();
	private StringBuilder contributorList = new StringBuilder();
	private StringBuilder definitionsList = new StringBuilder();
	private StringBuilder labelsList = new StringBuilder();
	private StringBuilder synonymsList = new StringBuilder();
	private String hostURL = "jdbc:virtuoso://terminologies.gfbio.org:1111";
	private String userName = "webservice";
	private String password = "E6:`7k#bzeTu9'W";

	public InfoExtractor(String terminology_url, String hostUrl,
			String username, String password) {
		this(terminology_url);
		this.hostURL = hostUrl;
		this.userName = username;
		this.password = password;
	}

	public InfoExtractor(String terminology_url) {
		this.terminology_url = terminology_url;
		this.extension = FilenameUtils.getExtension(terminology_url)
				.toLowerCase();
	}

	HashSet<String> foundProperties = new HashSet<String>();

	/**
	 * main method to conducting the extraction. decides if a skos or an owl
	 * file is present and and initiates the processing in that effect.
	 * 
	 * @return
	 */
	public HashMap<String, String> extractInfos()
			throws UnloadableImportException, OWLOntologyCreationException,
			SKOSCreationException, Exception {
		
		String fileFormat = checkOntologyFormatType();
		if (fileFormat.equals("skos")){
			initiateSKOS();
			putLists();
			if (map.isEmpty()){
				try {
					initiateOWL();
					putLists();
				}
				catch (Exception e){
					putDefaults();
				}
			}
		}
		else {
			initiateOWL();
			putLists();
			if (map.isEmpty()) {
				try {
					initiateSKOS();
					putLists();
				} catch (Exception e) {
					putDefaults();
					return map;
				}
			}
		}
		putDefaults();
		return map;
		}

	/**
	 * If there exists a previous version in the graph, we extract the
	 * previously entered metadata, because in most cases they don't change.
	 * 
	 * @param previousVersion
	 */
	private void getEnteredMetadata(String previousVersion) {
		String prefix = "http://purl.gfbio.org/";
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * from <" + prefix + "Metadata> WHERE { ");
		// mandatory input
		sb.append("<" + previousVersion + "> omv:description ?description; ");
		sb.append("omv:name ?name; ");
		sb.append("omv:hasDomain ?domain; ");
		sb.append("omv:hasFormalityLevel ?formalityLevel; ");
		sb.append("gfbio:relaseDate ?releaseDate; ");
		sb.append("omv:acronym ?acronym. ");

		// non-mandatory input
		sb.append("OPTIONAL {<" + previousVersion
				+ "> omv:creationDate ?creationDate}. ");
		sb.append("OPTIONAL {<" + previousVersion + "> omv:version ?version}. ");
		sb.append("OPTIONAL {<" + previousVersion
				+ "> omv:keywords ?keywords}. ");
		sb.append("OPTIONAL {<" + previousVersion
				+ "> gfbio:creators ?creators}. ");
		sb.append("OPTIONAL {<" + previousVersion
				+ "> gfbio:contributors ?contributors}. ");
		sb.append("OPTIONAL {<" + previousVersion
				+ "> omv:documentation ?documentation}. ");
		sb.append("OPTIONAL {<" + previousVersion
				+ "> gfbio:homepage ?homepage}. ");
		sb.append("OPTIONAL {<" + previousVersion
				+ "> omv:naturalLanguage ?natLanguage}. ");

		// non-mandatory input of contact data
		sb.append("OPTIONAL {<" + previousVersion
				+ "> omv:hasCreator ?creator. ");
		sb.append("?creator omv:firstName ?firstName. ");
		sb.append("?creator omv:lastName ?lastName. ");
		sb.append("?creator omv:eMail ?mail } }");

		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(
				sb.toString(), new VirtGraph(hostURL, userName, password));
		ResultSet result = vqe.execSelect();
		while (result.hasNext()) {
			QuerySolution next = result.next();
			// mandatory input
			RDFNode description = next.get("description");
			map.put("description", description.toString());
			RDFNode name = next.get("name");
			map.put("titles", name.toString());
			RDFNode domain = next.get("domain");
			map.put("domain", domain.toString());
			RDFNode formalityLevel = next.get("formalityLevel");
			map.put("formalityLevel", formalityLevel.toString());
			RDFNode acronym = next.get("acronym");
			map.put("acronym", acronym.toString());
			RDFNode releaseDate = next.get("releaseDate");
			String releaseDateString = releaseDate.toString();
			releaseDateString = releaseDateString.substring(0,
					releaseDateString.indexOf("^"));
			SimpleDateFormat now = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat after = new SimpleDateFormat("MM/dd/yyyy");
			try {
				Date date = now.parse(releaseDateString);
				String formattedDate = after.format(date);
				map.put("date_modified", formattedDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			// non-mandatory input
			RDFNode creationDate = next.get("creationDate");
			if (creationDate != null) {
				String creationDateString = creationDate.toString();
				creationDateString = creationDateString.substring(0,
						creationDateString.indexOf("^"));
				try {
					Date date = now.parse(creationDateString);
					String formattedDate = after.format(date);
					map.put("date_created", formattedDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			RDFNode version = next.get("version");
			if (version != null)
				map.put("version", version.toString());
			RDFNode keywords = next.get("keywords");
			if (keywords != null)
				map.put("keywords", keywords.toString());
			RDFNode documentation = next.get("documentation");
			if (documentation != null)
				map.put("documentation", documentation.toString());
			RDFNode homepage = next.get("homepage");
			if (homepage != null)
				map.put("homepage", homepage.toString());
			RDFNode naturalLanguage = next.get("natLanguage");
			if (naturalLanguage != null)
				map.put("language", naturalLanguage.toString());
			RDFNode creators = next.get("creators");
			if (creators != null)
				map.put("creators", creators.toString());
			RDFNode contributors = next.get("contributors");
			if (contributors != null)
				map.put("contributors", contributors.toString());
			RDFNode lastName = next.get("lastName");
			if (lastName != null)
				map.put("contact_lastname", lastName.toString());
			RDFNode firstName = next.get("firstName");
			if (lastName != null)
				map.put("contact_firstname", firstName.toString());
			RDFNode mail = next.get("mail");
			if (mail != null)
				map.put("contact_mail", mail.toString());

		}

	}

	/**
	 * A method to get the previous graph version of an uploaded ontology.
	 * 
	 * @return previous version of ontology or null if none exists
	 */
	private String getPreviousVersion() {
		String uri = map.get("uri");
		String prefix = "http://purl.gfbio.org/";
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ?id from <" + prefix + "Metadata> WHERE {");
		sb.append("?id omv:uri <" + uri + "> ; gfbio:graph ?graph}");
		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(
				sb.toString(), new VirtGraph(hostURL, userName, password));
		ResultSet result = vqe.execSelect();
		while (result.hasNext()) {
			QuerySolution next = result.next();
			RDFNode id = next.get("id");
			return id.toString();
		}
		return null;
	}

	/**
	 * construct the result map
	 */
	private void putLists() {
		if (!creators.isEmpty()) {
			for (String creator : creators) {
				creatorList.append(creator + ", ");
			}
			if (creatorList.length() > 0) {
				creatorList.deleteCharAt(creatorList.length() - 2);
				map.put("creators", creatorList.toString());
			}
		}
		if (!contributors.isEmpty()) {
			for (String creator : contributors) {
				contributorList.append(creator + ", ");
			}
			if (contributorList.length() > 0) {
				contributorList.deleteCharAt(contributorList.length() - 2);
				map.put("contributors", contributorList.toString());
			}
		}
		if (!definitions.isEmpty()) {
			for (String creator : definitions) {
				definitionsList.append(creator + ", ");
			}
			if (definitionsList.length() > 0) {
				definitionsList.deleteCharAt(definitionsList.length() - 2);
				map.put("definitions", definitionsList.toString());
			}
		}
		if (!labels.isEmpty()) {
			for (String creator : labels) {
				labelsList.append(creator + ", ");
			}
			if (labelsList.length() > 0) {
				labelsList.deleteCharAt(labelsList.length() - 2);
				map.put("labels", labelsList.toString());
			}
		}
		if (!synonyms.isEmpty()) {
			for (String creator : synonyms) {
				synonymsList.append(creator + ", ");
			}
			if (synonymsList.length() > 0) {
				synonymsList.deleteCharAt(synonymsList.length() - 2);
				map.put("synonyms", synonymsList.toString());
			}
		}

		String previousVersion = getPreviousVersion();
		if (previousVersion != null) {
			getEnteredMetadata(previousVersion);
		}

	}

	private void putDefaults() {
		if (definitions.isEmpty()) {
			map.put("definitions",
					"http://www.w3.org/2004/02/skos/core#definition");
		}
		if (labels.isEmpty()) {
			map.put("labels", "http://www.w3.org/2000/01/rdf-schema#label, "
					+ "http://www.w3.org/2004/02/skos/core#prefLabel");
		}
		if (synonyms.isEmpty()) {
			map.put("synonyms", "http://www.w3.org/2004/02/skos/core#altLabel");
		}
	}

	/**
	 * initiate the manager and find the properties in an owl file
	 */
	private void initiateOWL() throws UnloadableImportException,
			OWLOntologyCreationException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		IRI iri = IRI.create(terminology_url);
		OWLOntology ontology = null;

		try {
			ontology = manager.loadOntologyFromOntologyDocument(iri);
		} catch (UnloadableImportException e) {
			throw e;
		} catch (OWLOntologyCreationException e) {
			throw e;
		}

		String uri = ontology.getOntologyID().getOntologyIRI().toString();
		if (uri != null) {
			map.put("uri", uri);
		}
		for (OWLAnnotation an : ontology.getAnnotations()) {
			OWLAnnotationProperty prop = an.getProperty();
			IRI iriProp = prop.getIRI();
			String compare = iriProp.toString();
			OWLAnnotationValue valueAnn =  an.getValue();
			String value;
			try {
				value = ((OWLLiteral)valueAnn).getLiteral();
			}
			catch (Exception e){
				continue;
			}
			check(compare, value);
		}
		for (OWLClass c : ontology.getClassesInSignature()) {
			for (OWLAnnotation prop : c.getAnnotations(ontology)) {
				String property = prop.getProperty().toString();
				foundProperties.add(property);
			}
		}
		for (String prop_uri : foundProperties) {
			String query = "select ?label where {graph ?graph{"
					+ prop_uri
					+ " ?p ?label} . "
					+ "FILTER (?p = <http://www.w3.org/2000/01/rdf-schema#label>)}";
			VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(
					query.toString(),
					new VirtGraph(hostURL, userName, password));
			ResultSet label_result = vqe.execSelect();
			while (label_result.hasNext()) {
				String label = label_result.next().get("label").toString();
				if (prop_uri.startsWith("<")) {
					prop_uri = prop_uri.substring(1, prop_uri.length() - 1);
				}
				checkProperty(prop_uri, label);
			}
		}
		int owl = 0;
		int skos = 0;
		for (OWLClass an : ontology.getClassesInSignature()) {
			if (an.getAnnotations(ontology).toString().contains("rdfs:label")) {
				++owl;
			}
			if (an.getAnnotations(ontology).toString()
					.contains("skos:prefLabel")) {
				++skos;
			}
		}
		if (owl > skos) {
			map.put("ontoLang", "http://omv.ontoware.org/ontology#OWL");
		} else if (skos > owl) {
			map.put("ontoLang", "http://purl.org/gfbio/metadata-schema#SKOS");
		}
	}

	/**
	 * initiate the manager and find the properties in a skos file
	 */
	private void initiateSKOS() throws SKOSCreationException {
		SKOSManager manager;
		SKOSDataset dataset = null;
		try {
			manager = new SKOSManager();
			dataset = manager.loadDataset(URI.create(terminology_url));
		} catch (SKOSCreationException e) {
			throw e;
		}
		for (SKOSConceptScheme scheme : dataset.getSKOSConceptSchemes()) {
			Set<SKOSAnnotation> annos = scheme.getSKOSAnnotations(dataset);
			String uri = scheme.getURI().toString();
			if (uri != null) {
				map.put("uri", uri);
			}
			for (SKOSAnnotation anno : annos) {
				String compare = anno.getURI().toString();
				String property = compare.startsWith("http") ? "<" + compare
						+ ">" : compare;
				foundProperties.add(property);
				skosAnnotations.add(anno);
			}
		}
		for (SKOSAnnotation anno : skosAnnotations) {
			if (anno.getAnnotationValueAsConstant() != null) {
				String value = anno.getAnnotationValueAsConstant().getLiteral();
				check(anno.getURI().toString(), value);
			}
		}
		for (String prop_uri : foundProperties) {
			String query = "select ?label where {graph ?graph{"
					+ prop_uri
					+ " ?p ?label} . "
					+ "FILTER (?p = <http://www.w3.org/2000/01/rdf-schema#label>)}";
			VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(
					query.toString(),
					new VirtGraph(hostURL, userName, password));
			ResultSet label_result = vqe.execSelect();
			while (label_result.hasNext()) {
				String label = label_result.next().get("label").toString();
				if (prop_uri.startsWith("<")) {
					prop_uri = prop_uri.substring(1, prop_uri.length() - 1);
				}
				checkProperty(prop_uri, label);
			}
		}
		int owl = 0;
		int skos = 0;
		for (SKOSConcept con : dataset.getSKOSConcepts()) {
			for (SKOSAnnotation an : con.getSKOSAnnotations(dataset)) {

				if (an.getURI().toString()
						.contains("http://www.w3.org/2000/01/rdf-schema#label")) {
					++owl;
				}
				if (an.getURI()
						.toString()
						.contains(
								"http://www.w3.org/2004/02/skos/core#prefLabel")) {
					++skos;
				}
			}
		}

		if (owl > skos) {
			map.put("format",
					"http://omv.ontoware.org/2005/05/ontology#OWL-XML");
			map.put("ontoLang", "http://omv.ontoware.org/ontology#OWL");
		} else if (skos > owl) {
			map.put("format", "http://purl.org/gfbio/SKOS-XML");
			map.put("ontoLang", "http://purl.org/gfbio/metadata-schema#SKOS");
		}
	}

	/**
	 * checks if a property url is known and of use to us and adds it to the
	 * result lists if so.
	 * 
	 * @param compare
	 *            the property url
	 * @param value
	 *            the property value
	 */
	private void check(String compare, String value) {
		if (compare.equals("http://purl.org/dc/elements/1.1/title")
				|| compare.equals("http://purl.org/dc/terms/title")) {
			map.put("title", value.trim());
		}
		if (compare.equals("http://purl.org/dc/elements/1.1/description")
				|| compare.equals("http://purl.org/dc/terms/description")) {
			map.put("description", value.trim());
		}
		if (compare.equals("http://purl.org/dc/elements/1.1/creator")
				|| compare.equals("http://purl.org/dc/terms/creator")) {
			if (creators.isEmpty()) {
				creators = new HashSet<String>();
			}
			creators.add(value.trim());
		}
		if (compare.equals("http://purl.org/dc/elements/1.1/contributor")
				|| compare.equals("http://purl.org/dc/terms/contributor")) {
			if (contributors.isEmpty()) {
				contributors = new HashSet<String>();
			}
			contributors.add(value.trim());
		}
		if (compare.equals("http://purl.org/dc/terms/created")) {
			String format = "yyyy-MM-dd HH:mm";
			for (String regexp : TimeRegex.DATE_FORMAT_REGEXPS.keySet()) {
				if (value.toLowerCase().matches(regexp)) {
					format = TimeRegex.DATE_FORMAT_REGEXPS.get(regexp);
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			Date d = null;

			try {
				d = sdf.parse(value);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sdf.applyPattern("MM/dd/yyyy");
			map.put("date_modified", sdf.format(d).toString().trim());
		}
		if (compare.equals("http://purl.org/dc/terms/modified")) {
			String format = "yyyy-MM-dd HH:mm";
			for (String regexp : TimeRegex.DATE_FORMAT_REGEXPS.keySet()) {
				if (value.toLowerCase().matches(regexp)) {
					format = TimeRegex.DATE_FORMAT_REGEXPS.get(regexp);
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			Date d = null;
			try {
				d = sdf.parse(value);
				sdf.applyPattern("MM/dd/yyyy");
				map.put("date_modified", sdf.format(d).toString().trim());
			} catch (ParseException e) {

			}
		}
		if (compare.equals("http://purl.org/dc/elements/1.1/language")
				|| compare.equals("http://purl.org/dc/terms/language")) {
			map.put("language", value.trim());
		}
	}

	private void checkProperty(String property, String label) {
		if (label.toLowerCase().contains("definition")
				|| label.toLowerCase().contains("description")
				|| label.toLowerCase().contains("comment")
				&& label.equals("http://www.w3.org/2004/02/skos/core#definition")) {
			if (definitions.isEmpty()) {
				definitions = new HashSet<String>();
			}
			definitions.add(property.trim());
		}
		if (label.toLowerCase().contains("label")

				|| label.toLowerCase().contains("name")
				&& !label
						.equals("http://www.w3.org/2004/02/skos/core#prefLabel")
				&& !label.equals("http://www.w3.org/2000/01/rdf-schema#label")) {
			if (labels.isEmpty()) {
				labels = new HashSet<String>();
			}
			labels.add(property.trim());
		}
		if (label.toLowerCase().contains("synonym")
				&& !label
						.equals("http://www.w3.org/2004/02/skos/core#altLabel")) {
			if (synonyms.isEmpty()) {
				synonyms = new HashSet<String>();
			}
			synonyms.add(property.trim());
		}
	}

	public void setUserName(String name) {
		this.userName = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setHostUrl(String hostUrl) {
		this.hostURL = hostUrl;
	}
	
	/**
	 * This methods determines whether the given ontology is a skos or owl file
	 * by checking the axioms class expressions
	 * 
	 * @return ontology format owl or skos
	 * @throws Exception 
	 */
	private String checkOntologyFormatType() throws Exception {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		IRI iri = IRI.create(terminology_url);
		try {
			OWLOntology loadedOntology = manager.loadOntology(iri);
			Set<OWLAxiom> aBoxAxioms = loadedOntology.getTBoxAxioms(false);
			Iterator<OWLAxiom> iterator = aBoxAxioms.iterator();
			while (iterator.hasNext()){
				OWLAxiom next = iterator.next();
				Set<OWLClassExpression> nestedClassExpressions = next.getNestedClassExpressions();
				Iterator<OWLClassExpression> iterator2 = nestedClassExpressions.iterator();
				while (iterator2.hasNext()){
					OWLClassExpression next2 = iterator2.next();
					boolean equals = next2.toString().equals("<http://www.w3.org/2004/02/skos/core#Concept>");
					if (equals){
						return "skos";
					}
					else {
						return "owl";
					}
				}
			}
			return "owl";
		} catch (OWLOntologyCreationException e) {
			throw new Exception("Unable to load ontology.");
		}
	}
	
}
